# `style/css` Directory
The `style/css` directory contains all compiled CSS for your theme. 
This is where the actual styles live that are loaded when this theme is used to render a Drupal page.
Note the relevant portion of `iti_omega.libraries.yml`:

```
iti_omega:
  js:
    js/iti_omega.js: {}
  css:
    theme:
      style/css/iti_omega.css: {}
```
