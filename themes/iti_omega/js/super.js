$(document).ready(function() {
   $('*').each(function() {
    var n = $(this).html().replace(
        new RegExp('®','g'),
        '<sup>®</sup>'
        ).replace(
        new RegExp('&reg;','g'),
        '<sup>&reg;</sup>'
        );
    $(this).html(n);
  });
});​