(function ($) {
	Drupal.behaviors.myModuleBehavior = {
		attach: function (context, settings) {
			$('h1,h2,h3,p,a,span,li,dl,td').each(function(i, elem) {
            $(elem).html(function(i, html) {
                return html.replace(/\u00ae/g, "<sup>&reg;</sup>");
            });
            $(elem).html(function(i, html) {
                return html.replace(/\u00a9/g, "<sup>&copy;</sup>");
            });
        });
		}
	};
})(jQuery);
